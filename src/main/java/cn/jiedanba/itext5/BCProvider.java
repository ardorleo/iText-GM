package cn.jiedanba.itext5;

import java.security.Provider;
import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class BCProvider {

	protected static final Provider BC = new BouncyCastleProvider();

	static {
		Security.removeProvider("SunEC");
		Security.addProvider(BC);

	}
}
