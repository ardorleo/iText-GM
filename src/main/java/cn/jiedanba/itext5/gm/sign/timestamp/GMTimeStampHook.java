package cn.jiedanba.itext5.gm.sign.timestamp;

import org.ofdrw.sign.timestamp.TimeStampHook;

import com.itextpdf.text.pdf.security.TSAClient;

/**
 * 国密时间戳签名
 *
 */
public class GMTimeStampHook implements TimeStampHook {

	private TSAClient tsaClient;

	public GMTimeStampHook(TSAClient tsaClient) {
		this.tsaClient = tsaClient;
	}

	@Override
	public byte[] apply(byte[] signature) {
		try {
			byte[] timeSign = tsaClient.getTimeStampToken(signature);
			return timeSign;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("时间戳签名异常", e);
		}
	}

}
