package cn.jiedanba.itext5.gm.sign.vo;

import java.io.Serializable;

import org.ofdrw.gm.ses.v4.TBS_Sign;

import lombok.Data;

@Data
public class GetPdfHash implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 空签名域的PDF
	 */
	private byte[] emptySignaturePdf;

	/**
	 * pdf 原文摘要值
	 */
	private byte[] digesHash;

	/**
	 * 待签p1的摘要值
	 */
	private byte[] signHash;

	/**
	 * 签名域标识
	 */
	private String fieldName;

	/**
	 * 待签p1的值
	 */
	private TBS_Sign TBS_Sign;
}
