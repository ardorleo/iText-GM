package cn.jiedanba.itext5.gm.sign.vo;

import java.io.Serializable;

import org.ofdrw.gm.ses.v4.SESeal;

import lombok.Data;

@Data
public class GetPdfHashParamVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 待签名pdf
	 */
	private byte[] pdf;

	/**
	 * 电子印章数据
	 */
	private SESeal seal;

	/**
	 * 签名理由
	 */
	private String reason;

	/**
	 * 签名位置
	 */
	private String location;

	/**
	 * 签名页码
	 */
	private int pageNo;
	/**
	 * 图章左下角x
	 */
	private float llx;
	/**
	 * 图章左下角y
	 */
	private float lly;
	/**
	 * 图章右上角x
	 */
	private float urx;

	/**
	 * 图章右上角y
	 */
	private float ury;

	/**
	 * 摘要算法。SHA1，SHA256
	 */
	private String hashAlgorithm;

	/**
	 * 签名图片
	 */
	private byte[] signImage;
}
