package cn.jiedanba.itext5.signatureContainer;

import java.io.InputStream;

import com.itextpdf.text.pdf.PdfDictionary;
import com.itextpdf.text.pdf.security.ExternalSignatureContainer;

public class MyExternalSignatureContainer implements ExternalSignatureContainer {
	protected byte[] sig;

	public MyExternalSignatureContainer(byte[] sig) {
		this.sig = sig;
	}

	@Override
	public byte[] sign(InputStream is) {
		return sig;
	}

	@Override
	public void modifySigningDictionary(PdfDictionary signDic) {
	}
}
