package cn.jiedanba.itext5.signhash.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;

import cn.jiedanba.itext5.gm.sign.ITextGM;
import cn.jiedanba.itext5.gm.sign.vo.GetPdfHash;
import cn.jiedanba.itext5.gm.sign.vo.GetPdfHashParamVo;
import cn.jiedanba.itext5.rsa.signHash.ITextSignHashUtil;
import cn.jiedanba.itext5.util.PkiUtil;

/**
 * pdf分离式签名 . 场景：从外部设备获取p1数据，例如ukey，签验签服务器
 * 
 * @author dell
 *
 */
public class SignHashTest {

	private static String pkStr = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCj57c/PKjzqe8SjgLuqEApjV5r6XmgRQaUxf8ABqHvBSOHwrdROXy16+SdR26E3UgUSWcsCpg+ZT/FiqxSqwoRbYbfGC4i7iqo3sZV+y1mbDAOb4mayd2DkPq4g8i0XtkgwgJgcXXBMublXOs4MPeuoLdsWLjzI9X1NK4jrQXYWaq2H8N9yM24x6alxg32AhObavXNEB9v/LYHPIBh2wdd5otJeFJZmLBk3ZUJ4/+9xJzFXqcUtM1L1RNoHIcwGkh57ez28vdH5Z1QoGzZNOOXePKtM5WFkc5xFQB33dLzaT/0BYTM6ELwwershvckZ+laORAk6VyPtyMsuov+pKunAgMBAAECggEAWgHEEnjoOq6F98V+fLpuSaM1R8M1pDpji81S5BvATzaeNU5LRtJv1qMMrwXr/hBaJbHNc1T6F8GYDI3DZb5BUCK6Xsazh0EMsiFr/wlo/AbLORItGZBjEX3LAQp1a/9t8tw3W+jShl7Chw/Ip9nUGdRuBZSPNA3oz7e3LSY1wIFTv5nAJcC3OLoS+STN7rrnKh4tdqH3pZSp6o6ffl8yttSlSADFOIayKBlv/+NCT2c/avnnEBAf2vu8BWP0pnnf3Ftk8Aq3uea+YHZ/xPjpVYVaQBfLiroUNk5aFhVm7H5fAUHKcVJRuHXEGYUmfbBJXrcKjrZuhdaUFAbqcByLAQKBgQDYAnRJHayN/HqkaoZSSiAKI/WUtzJl3/g1BF4VZ5UozV8de1I00FrL2aWQ72x1nHhaeFBLGCnR/Fie5a8QrMjqYtuIKZ5zNc1CDLkIENO0U4n850f/Dpkr3c4oC9iLVCSB5zwBwG2seB/bxZH9KxZDO82bGaL8GcwoVBZj1atigQKBgQDCP9OR/rzRPBFJp4WXlPjkiMi233tjiaf2tL80VVqGmCi1J21nMvyWgzZO4g6IJ6yuSxVbx8vrYD964dEge1jkGHxO9+Je5GraugrNTU7PD+4xhYQpxvLWkheIOxW5wz0BttMWqa8j8uvR6XCBdGUCxsYoIT2lqfGYh3xedJ+qJwKBgQC+bsN35ipG7sxsgOE0UNcYOYV6+1r6B0755oTPWAde9lehVSQvHXS+fH2DXiB/O1e7YpBe88mCr3atdw40pC9ou4iQ7Sgcp9TyIudJHhSVyIRBKuEFEiilTKFAGtloU+DviR0U6kGGKEzl4rMGv+KbmXB1CU/v8wpSj3dBwpCagQKBgFMyDQZ5iRtAU9Ms+a2rc1JGN1kDrJA8NAEY9s+OQwBkiQLViSjrEH3SYt80OK8d9vqLU/GESBVemrGhp/T2f2LqDcuwpXao9A8MIXY+xyMuGoGBQtK1z+oQlh2V2L9AHI/wYsZRFJ8b+t5j0fjQBjdDNTmYyxN6DuyaEq1N29h/AoGBAI6T7Qu3O5eU0TRKlK3lSYgaYpKZU6S1Q5odeOOoGpOveWyg3KBh/0dBz/Mp1v7yVcPj8gBkXmCEC8pIuk0HPbbM6K4OZO02FT9sX1fNVK/mqm1PYeLD1sg2tihYah5WRSo2BssndfsIxHuYvzouvil2XDKiEBk749KxPkWLxi2s";

	private static String certStr = "MIIFUDCCBDigAwIBAgIKaCaz+QuJ7f/PwTANBgkqhkiG9w0BAQsFADBoMQswCQYDVQQGEwJDTjE2MDQGA1UECgwtRGlnaXRhbCBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkgQ2VudGVyIENvLiwgTHRkMSEwHwYDVQQDDBhHbG9iYWwgSWRlbnRpdHkgQ0EgLSBSU0EwHhcNMjIwODA4MTcxMDQ2WhcNMjQwODA3MTcxMDQ2WjCBozELMAkGA1UEBhMCQ04xEjAQBgNVBAgMCeW5v+S4nOecgTESMBAGA1UEBwwJ5rex5Zyz5biCMScwJQYDVQQKDB7mt7HlnLPmtYvor5Xnp5HmioDmnInpmZDlhazlj7gxGjAYBgNVBAsMETkxNDQwMzIwNzI4NTg2NDZUMScwJQYDVQQDDB7mt7HlnLPmtYvor5Xnp5HmioDmnInpmZDlhazlj7gwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCj57c/PKjzqe8SjgLuqEApjV5r6XmgRQaUxf8ABqHvBSOHwrdROXy16+SdR26E3UgUSWcsCpg+ZT/FiqxSqwoRbYbfGC4i7iqo3sZV+y1mbDAOb4mayd2DkPq4g8i0XtkgwgJgcXXBMublXOs4MPeuoLdsWLjzI9X1NK4jrQXYWaq2H8N9yM24x6alxg32AhObavXNEB9v/LYHPIBh2wdd5otJeFJZmLBk3ZUJ4/+9xJzFXqcUtM1L1RNoHIcwGkh57ez28vdH5Z1QoGzZNOOXePKtM5WFkc5xFQB33dLzaT/0BYTM6ELwwershvckZ+laORAk6VyPtyMsuov+pKunAgMBAAGjggG+MIIBujAOBgNVHQ8BAf8EBAMCBsAwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBTKKyYUmiwIPqj5U1rbdtKEMZZz8zAdBgNVHQ4EFgQUwvtJ/GDLRxTjVP/352EDnTNp6gAwFAYKKwYBBAHWeQIEAgQGFgRUZXN0MCwGA1UdJQEB/wQiMCAGCisGAQQBgjcKAwwGCCsGAQUFBwMEBggrBgEFBQcDAjBEBgNVHR8EPTA7MDmgN6A1hjNodHRwOi8vY3JsLmppZWRhbmJhLmNuL2NybC9HbG9iYWxJZGVudGl0eUNBLVJTQS5jcmwwgYMGCCsGAQUFBwEBBHcwdTAyBggrBgEFBQcwAYYmaHR0cDovL29jc3AuamllZGFuYmEuY24vb2NzcC9vY3NwUXVlcnkwPwYIKwYBBQUHMAKGM2h0dHA6Ly9jcnQuamllZGFuYmEuY24vY3J0L0dsb2JhbElkZW50aXR5Q0EtUlNBLmNydDBKBgNVHSAEQzBBMDUGCWCGSAGG/WwBATAoMCYGCCsGAQUFBwIBFhpodHRwOi8vY3BzLmppZWRhbmJhLmNuL2NwczAIBgZngQwBAgIwDQYJKoZIhvcNAQELBQADggEBACEqZUwxanNeSuzcHJvIqcgbyI3o+D0Er2nrkNWJjU66gFLtUuJ3dRDR7LxNQpZPOeKX87l7sGz9U8BIMRWfdjj8eT8RPHL0015awUvHp1k1MX6vf979O6PVnUC0jEOishkCkhMvuS6QYa9BZHpT4M6cw8B0tSGGLVCaKRByz5/k+PRv0R6QGzFJj/yaGBv6ZIv7++dIiqe8MfVP4pbB+l4lGIJfLcM50HHWiCiGDwW49KmGb1jrrtginrnadxKBm22AjLZRblQ/Ugw8PNqIYRtKhjETLPnTBLZOalfskKZHt/vG8emXKptlgSCyZDJXSy38WbbN7Yzr/SOZQClEKE4=";

	public static void sign() throws IOException, GeneralSecurityException, DocumentException {
		GetPdfHashParamVo paramVo = new GetPdfHashParamVo();
		Path pdf = Paths.get("src/main/resources", "test.pdf");
		paramVo.setPdf(Files.readAllBytes(pdf));
		paramVo.setPageNo(1);
		paramVo.setLlx(240);
		paramVo.setLly(290);
		paramVo.setUrx(paramVo.getLlx() + 120);
		paramVo.setUry(paramVo.getLly() + 120);
		paramVo.setLocation("深圳南山区");
		paramVo.setReason("分离式签名测试");
		paramVo.setHashAlgorithm("SHA256");
		Path sealImage = Paths.get("src/main/resources", "深圳测试科技有限公司_公章.png");
		paramVo.setSignImage(Files.readAllBytes(sealImage));

		// 创建签名域，获取pdf文件摘要
		GetPdfHash getPdfHash = ITextSignHashUtil.getPdfHash(paramVo);

		TSAClient tsaClient = new TSAClientBouncyCastle("http://timestamp.gdca.com.cn/tsa", null, null, 4096, "SHA256");

		PrivateKey prvKey = PkiUtil.getPrivateKey(Base64.decodeBase64(pkStr));

		X509Certificate signCert = PkiUtil.readX509Certificate(Base64.decodeBase64(certStr));

		// 签署p1，以下为模拟外部签名测试
		byte[] p1 = PkiUtil.sign(prvKey, "SHA256WithRSA", getPdfHash.getSignHash());

		// 签署p7
		byte[] p7 = ITextSignHashUtil.signHash(getPdfHash.getDigesHash(), p1,
				PkiUtil.getCertificateChain(signCert.getEncoded()), "SHA256", tsaClient);

		// 签署pdf
		byte[] signSuccess = ITextGM.signDeferred(getPdfHash.getEmptySignaturePdf(), p7, getPdfHash.getFieldName());

		FileUtils.writeByteArrayToFile(new File("src/main/resources/sign_hash.pdf"), signSuccess);
	}

	public static void main(String[] args) throws IOException, GeneralSecurityException, DocumentException {
		sign();
	}
}
